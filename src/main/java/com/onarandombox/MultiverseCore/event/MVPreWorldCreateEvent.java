package com.onarandombox.MultiverseCore.event;

import org.bukkit.WorldCreator;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Called before a new world is created.
 */
public class MVPreWorldCreateEvent extends Event implements Cancellable {

	private final WorldCreator worldCreator;
	private boolean cancelled;

	public MVPreWorldCreateEvent(WorldCreator worldCreator) {
		if (worldCreator == null)
			throw new IllegalArgumentException("worldCreator cannot be null!");

		this.worldCreator = worldCreator;
	}

	private static final HandlerList HANDLERS = new HandlerList();

	/**
	 * Get's the WorldCreator that is about to be used to create a world.
	 *
	 * @return That {@link WorldCreator}
	 */
	public WorldCreator getWorldCreator() {
		return worldCreator;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HandlerList getHandlers() {
		return HANDLERS;
	}

	/**
	 * Gets the handler list. This is required by the event system.
	 *
	 * @return A list of HANDLERS.
	 */
	public static HandlerList getHandlerList() {
		return HANDLERS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCancelled() {
		return this.cancelled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

}
